use super::SpyScheduler;
use crate::{
    domain::data::DomainBankAccount,
    ingestor::Ingestor,
    scheduler::factory_trait::{error::Result, SchedulerFactory},
};
use std::sync::{Arc, Mutex};

#[derive(Clone, Default)]
pub struct SpySchedulerFactory {
    accounts: Arc<Mutex<Vec<DomainBankAccount>>>,
    ingestors: Arc<Mutex<Vec<Arc<dyn Ingestor>>>>,
}

impl SpySchedulerFactory {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_ingestors(&self) -> Vec<Arc<dyn Ingestor>> {
        self.ingestors.lock().unwrap().to_owned()
    }

    pub fn get_accounts(&self) -> Vec<DomainBankAccount> {
        self.accounts.lock().unwrap().to_owned()
    }

    pub fn was_called(&self) -> bool {
        !self.get_accounts().is_empty()
    }
}

impl SchedulerFactory for SpySchedulerFactory {
    type Scheduler = SpyScheduler;

    fn create(
        &self,
        ingestor: crate::ingestor::IngestionHandle,
        bank_account: &DomainBankAccount,
    ) -> Result<Self::Scheduler> {
        self.accounts.lock().unwrap().push(bank_account.clone());
        self.ingestors.lock().unwrap().push(ingestor);
        Ok(SpyScheduler::default())
    }
}
