use crate::scheduler::scheduler_trait::{error::Result, Scheduler};
use std::sync::{
    atomic::{AtomicU32, Ordering},
    Arc,
};

#[derive(Debug, Default, Clone)]
pub struct SpyScheduler {
    num_calls: Arc<AtomicU32>,
}

impl SpyScheduler {
    pub fn get_num_synchronize_calls(&self) -> u32 {
        self.num_calls.load(Ordering::SeqCst)
    }
}

#[async_trait::async_trait]
impl Scheduler for SpyScheduler {
    async fn request_synchronize(&self) -> Result<()> {
        self.num_calls.fetch_add(1, Ordering::SeqCst);
        Ok(())
    }
}
