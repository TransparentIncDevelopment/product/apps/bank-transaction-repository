pub mod error;

use crate::{domain::data::DomainHistoryRequest, Transaction};
use error::Result;

#[async_trait::async_trait]
pub trait HistoryPuller {
    async fn get_history(&self, request: &DomainHistoryRequest) -> Result<Vec<Transaction>>;
}
