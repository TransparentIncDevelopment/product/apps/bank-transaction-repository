use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

pub struct DummySecretStore;

#[async_trait::async_trait]
impl SecretKeyValueStore for DummySecretStore {
    async fn read(&self, _key: &str) -> Result<Secret<String>, ReadSecretError> {
        unimplemented!()
    }

    async fn check_health(&self) -> Result<(), CheckHealthError> {
        unimplemented!()
    }
}
