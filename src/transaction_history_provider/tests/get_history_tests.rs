use crate::{
    domain::data::{DomainBankAccount, DomainHistoryRequest, DomainTransaction},
    store::{
        backends::spy_store::{transaction_client_impl::QueryCallRecord, SpyStore},
        client::transaction_store::{
            date_range::{DateRange, DateRangePolicy},
            query::error::Error as QueryError,
        },
    },
    synchronizer::synchronizer_trait::error::Error as SynchronizerError,
    transaction_history_provider::{
        events::HistoryRequested,
        history_puller::{error::Error as HistoryPullerError, HistoryPuller},
        tests::{DummySecretStore, MockTransactionHistoryProviderDependencies, TestError},
        TransactionHistoryProvider,
    },
    BankConfiguration,
};
use assert_matches::assert_matches;
use chrono::Utc;
use itertools::Itertools;
use std::sync::Arc;
use tokio::runtime::Runtime;
use tracing;
use tracing_assert_core::{self, debug_fmt_ext::DebugFmtExt};
use tracing_assert_macros::tracing_capture_event_fields;

fn create_single_account_bank() -> (BankConfiguration, DomainBankAccount) {
    let bank = BankConfiguration::test_with(0, 1);
    let (account,) = bank.get_domain_bank_accounts().into_iter().collect_tuple().unwrap();
    (bank, account)
}

#[tokio::test]
async fn get_history__calls_synchronize_with_correct_bank_account() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a history provider
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store.clone(),
        store,
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request
    let request = DomainHistoryRequest::new(account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let _transactions = provider.get_history(&request).await.unwrap();

    // Then
    let synchronizer = deps.get_synchronizer(&bank.routing_number).unwrap();
    assert_eq!(synchronizer.get_synchronize_calls(), vec![account]);
}

#[tokio::test]
async fn get_history__returns_error_for_missing_synchronizer() {
    // Given a bank with an account
    let bank = BankConfiguration::test_with(0, 1);

    // And a history provider
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store.clone(),
        store,
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request for an account that wasn't added
    let absent_account = DomainBankAccount::test();
    let request = DomainHistoryRequest::new(absent_account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let result = provider.get_history(&request).await;

    // Then
    assert_matches!(
        result,
        Err(HistoryPullerError::MissingBank { routing_number }) if routing_number == absent_account.get_routing_number()
    );
}

#[tokio::test]
async fn get_history__returns_error_when_synchronize_returns_error() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a history provider
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store.clone(),
        store,
        &deps,
        secret_store,
    )
    .unwrap();

    // And an synchronizer configured to error
    deps.get_synchronizer(&bank.routing_number)
        .unwrap()
        .set_synchronize_result(Err(SynchronizerError::SynchronizeError(Box::new(TestError::Test))));

    // And a history request
    let request = DomainHistoryRequest::new(account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let actual_error = provider.get_history(&request).await;

    // Then
    assert_matches!(actual_error, Err(HistoryPullerError::GetHistoryError(..)));
}

#[tokio::test]
async fn get_history__returns_query_results() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a store
    let store_client = Arc::new(SpyStore::default());

    // And a list of stored transactions returned by the store
    let expected_transactions = vec![
        DomainTransaction::test_with_metadata("0"),
        DomainTransaction::test_with_metadata("1"),
    ];
    store_client
        .transaction_client_calls
        .set_query_result(Ok(expected_transactions.clone()));

    // And a history provider
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store_client.clone(),
        store_client,
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request
    let request = DomainHistoryRequest::new(account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let actual_transactions = provider.get_history(&request).await.unwrap();

    // Then
    assert_eq!(
        actual_transactions,
        expected_transactions.into_iter().map(Into::into).collect::<Vec<_>>(),
    );
}

#[tokio::test]
async fn get_history__query_is_passed_correct_arguments() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a history provider
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store.clone(),
        store.clone(),
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request
    let request = DomainHistoryRequest::new(account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    provider.get_history(&request).await.unwrap();

    // Then
    let expected_query_args = QueryCallRecord {
        bank_account: account,
        date_range: DateRange::new_unterminated(request.get_history_after()),
        date_range_policy: DateRangePolicy::Early,
    };
    assert_eq!(
        store.transaction_client_calls.get_query_calls(),
        vec![expected_query_args],
    );
}

#[tokio::test]
async fn get_history__returns_error_when_query_returns_error() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a Store that errors
    let store = Arc::new(SpyStore::default());
    store
        .transaction_client_calls
        .set_query_result(Err(QueryError::BackendError(Box::new(TestError::Test))));

    // And a history provider
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank.clone()].into_iter().collect(),
        store.clone(),
        store,
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request
    let request = DomainHistoryRequest::new(account.clone(), chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let actual_error = provider.get_history(&request).await;

    // Then
    assert_matches!(actual_error, Err(HistoryPullerError::GetHistoryError(..)));
}

#[test]
fn get_history__emits_event_HistoryRequested() {
    // Given a bank with an account
    let (bank, account) = create_single_account_bank();

    // And a Store that errors
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(
        &vec![bank].into_iter().collect(),
        store.clone(),
        store,
        &deps,
        secret_store,
    )
    .unwrap();

    // And a history request
    let request = DomainHistoryRequest::new(account, chrono::DateTime::<Utc>::MIN_UTC);

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(provider.get_history(&request)).unwrap();
    });

    // Then
    let expected_event: Vec<(String, String)> =
        vec![("message".into(), HistoryRequested { request: &request }.debug_fmt())];
    assert!(events.contains(&expected_event), "{:?}", events);
}
