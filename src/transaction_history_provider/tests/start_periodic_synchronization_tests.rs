use crate::{
    periodic::Periodic,
    public_interface::BankConfiguration,
    store::backends::spy_store::SpyStore,
    transaction_history_provider::{
        events::PeriodicSynchronizationStarted,
        tests::{mock_dependencies::MockTransactionHistoryProviderDependencies, DummySecretStore},
        TransactionHistoryProvider,
    },
};
use std::sync::Arc;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;

#[test]
fn start_sync__emits_started_event() {
    // Given
    let banks = BankConfiguration::test_many_with(2, 3).into_iter().collect();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);
    let provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // When
    let events = tracing_capture_event_fields!({
        provider.start_periodic_synchronization().unwrap();
    });

    // Then
    let expected_event: Vec<(String, String)> = vec![("message".into(), PeriodicSynchronizationStarted().debug_fmt())];
    assert!(events.contains(&expected_event), "{:?}", events);
}
