use crate::ingestor::xand_banks_bank_client::error::Error as XandBanksBankClientError;
use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    BankClientCreationError(#[from] XandBanksBankClientError),
    #[error(transparent)]
    InitializationError(Box<dyn StdError + Send + Sync + 'static>),
}
