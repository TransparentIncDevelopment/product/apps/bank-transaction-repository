use std::time::Duration;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Timer has already been started")]
    TimerAlreadyStarted,
    #[error("Given period {:?} must not be less than cooldown {:?}", .period, .cooldown)]
    PeriodLessThanCooldown { period: Duration, cooldown: Duration },
}

#[cfg(test)]
mod tests {
    use super::Error;
    use std::time::Duration;

    #[test]
    fn start_timer_error_display__period_less_than_cooldown_displays_appropriate_times() {
        // Given
        let error = Error::PeriodLessThanCooldown {
            period: Duration::from_secs(22),
            cooldown: Duration::from_secs(75),
        };

        // When
        let display_str = format!("{}", error);

        // Then
        assert_eq!(display_str, "Given period 22s must not be less than cooldown 75s");
    }
}
