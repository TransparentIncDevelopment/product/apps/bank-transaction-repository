use chrono::{DateTime, Utc};

pub fn get_event_value(events: Vec<tracing_assert_core::FieldValueMap>) -> String {
    events
        .into_iter()
        .map(|vec| vec.into_iter().map(|(_msg, detail)| detail).collect::<String>())
        .collect::<String>()
}

pub fn parse_time(time: &str) -> DateTime<Utc> {
    DateTime::parse_from_rfc3339(time).unwrap().into()
}
