mod spy_bank_adapter;

use crate::{
    domain::data::{BankTransaction, DomainBankAccount},
    ingestor::{
        bank_client::{error::Error, BankClient},
        bank_client_implementations::xand_banks_bank_client::{
            tests::spy_bank_adapter::{HistoryCallRecord, SpyBankAdapter},
            XandBanksBankClient, NINETY_DAYS,
        },
    },
};
use assert_matches::assert_matches;
use chrono::Duration;
use xand_banks::{
    date_range::DateRange,
    errors::XandBanksErrors,
    models::{BankTransaction as XandBankTransaction, BankTransactionType},
    BankAdapter,
};
use xand_money::Usd;

impl XandBanksBankClient {
    fn test_with(bank_client: Box<dyn BankAdapter>) -> Self {
        Self { bank_client }
    }
}

#[tokio::test]
async fn get_transaction_history__returns_bank_client_history_results() {
    // Given a bank adapter
    let bank_adapter = SpyBankAdapter::default();

    // And a set of transactions to be returned by the adapter's history function
    let bank_transactions = vec![
        XandBankTransaction {
            bank_unique_id: "transaction_1".to_string(),
            amount: Usd::default(),
            metadata: "some_metadata".to_string(),
            txn_type: BankTransactionType::Debit,
        },
        XandBankTransaction {
            bank_unique_id: "transaction_2".to_string(),
            amount: Usd::default(),
            metadata: "some__other_metadata".to_string(),
            txn_type: BankTransactionType::Credit,
        },
    ];
    bank_adapter.set_result(Ok(bank_transactions.clone()));

    // And a XandBanksBankClient
    let bank_client = XandBanksBankClient::test_with(Box::new(bank_adapter));

    // And some bank account
    let bank_account = DomainBankAccount::test();

    // When
    let result = bank_client.get_transaction_history(&bank_account).await.unwrap();

    // Then
    let expected_result: Vec<BankTransaction> = bank_transactions.into_iter().map(Into::into).collect();
    assert_eq!(result, expected_result);
}

#[tokio::test]
async fn get_transaction_history__history_is_passed_correct_arguments() {
    // Given a XandBanksBankClient
    let bank_adapter = SpyBankAdapter::default();
    let bank_client = XandBanksBankClient::test_with(Box::new(bank_adapter.clone()));

    // And some bank account
    let bank_account = DomainBankAccount::test();

    // When
    bank_client.get_transaction_history(&bank_account).await.unwrap();

    // Then
    let actual_history_arguments = bank_adapter.get_history_calls()[0].clone();
    let expected_history_arguments = HistoryCallRecord {
        account_number: bank_account.get_account_number(),
        date_range: DateRange::from_past_to_now(Duration::days(NINETY_DAYS)).unwrap(),
    };
    assert_eq!(
        actual_history_arguments.account_number,
        expected_history_arguments.account_number,
    );
    assert!(
        expected_history_arguments.date_range.begin_date() - actual_history_arguments.date_range.begin_date()
            < Duration::seconds(3)
            && expected_history_arguments.date_range.end_date() - actual_history_arguments.date_range.end_date()
                < Duration::seconds(3)
    );
}

#[tokio::test]
async fn get_transaction_history__returns_error_when_history_returns_error() {
    // Given a bank adapter erroring on date range creation
    let bank_adapter = SpyBankAdapter::default();
    bank_adapter.set_result(Err(XandBanksErrors::Mock));

    // And a XandBanksBankClient
    let bank_client = XandBanksBankClient::test_with(Box::new(bank_adapter.clone()));

    // And some bank account
    let bank_account = DomainBankAccount::test();

    // When
    let result = bank_client.get_transaction_history(&bank_account).await;

    // Then
    assert_matches!(result.unwrap_err(), Error::HistoryFetchError(..));
}

#[test]
fn get_transaction_history__emits_error_event_when_history_returns_error() {
    use tokio::runtime::Runtime;
    use tracing_assert_macros::tracing_capture_event_fields;

    // Given a bank adapter erroring on date range creation
    let expected_inner_error = XandBanksErrors::General {
        message: "MCB failed".to_string(),
    };
    let expected_error = Error::HistoryFetchError(Box::new(expected_inner_error.clone()));
    let bank_adapter = SpyBankAdapter::default();
    bank_adapter.set_result(Err(expected_inner_error));

    // And a XandBanksBankClient
    let bank_client = XandBanksBankClient::test_with(Box::new(bank_adapter));

    // And some bank account
    let bank_account = DomainBankAccount::test();

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(bank_client.get_transaction_history(&bank_account))
            .unwrap_err();
    });

    //Then
    let expected_event: Vec<(String, String)> = vec![("error".into(), expected_error.to_string())];
    assert!(events.contains(&expected_event), "{:?}", events);
}
