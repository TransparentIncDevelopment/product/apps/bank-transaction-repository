use crate::domain::data::DomainTransaction;
use crate::{domain::data::DomainBankAccount, ingestor::differ::TransactionDifferenceDetail};
use chrono::{DateTime, Utc};
use std::fmt::Debug;

#[derive(Debug)]
pub struct IngestionStarted<'a> {
    pub ingestion_time: &'a DateTime<Utc>,
    pub bank_account: &'a DomainBankAccount,
}

#[derive(Debug)]
pub struct IngestionCompleted<'a> {
    pub ingestion_time: &'a DateTime<Utc>,
    pub bank_account: &'a DomainBankAccount,
}

#[derive(Debug)]
pub struct BankTransactionsRetrieved<'a> {
    pub total: usize,
    pub new: usize,
    pub bank_account: &'a DomainBankAccount,
}

#[derive(Debug)]
pub struct DuplicateTransactionsInHistory {
    pub transactions: Vec<DomainTransaction>,
}

#[derive(Debug)]
pub struct DifferenceInTransactions<'a> {
    pub diff: &'a TransactionDifferenceDetail,
}

#[derive(Debug)]
pub struct FailedToInsertForensic {
    pub bank_txn_id: String,
    pub synthetic_id: String,
    pub timestamp: DateTime<Utc>,
    pub error: String,
}
