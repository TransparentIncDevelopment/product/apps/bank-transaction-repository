use crate::{domain::data::DomainBankAccount, store::backends::sqlite_database::db::schema::account};

#[derive(Debug, Clone, Insertable, PartialEq, Eq)]
#[table_name = "account"]
pub struct NewAccount {
    pub routing_number: String,
    pub account_number: String,
}

impl NewAccount {
    pub const fn new(routing_number: String, account_number: String) -> Self {
        Self {
            routing_number,
            account_number,
        }
    }
}

impl From<DomainBankAccount> for NewAccount {
    fn from(bank_account: DomainBankAccount) -> Self {
        Self {
            routing_number: bank_account.get_routing_number(),
            account_number: bank_account.get_account_number(),
        }
    }
}
