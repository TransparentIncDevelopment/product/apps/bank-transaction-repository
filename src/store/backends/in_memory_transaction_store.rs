use crate::{
    domain::data::{DomainBankAccount, DomainTransaction, IngestionRecord, TimestampedTransaction},
    store::client::transaction_store::{
        date_range::{DateRange, DateRangePolicy},
        insert::{error::Result as InsertResult, TransactionStoreInsertClient},
        query::{error::Result as QueryResult, TransactionStoreQueryClient},
    },
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use futures::lock::Mutex;
use std::{collections::HashMap, sync::Arc};

#[derive(Default)]
struct AccountHistory {
    txns: Vec<TimestampedTransaction>,
    last_ingest_time: Option<DateTime<Utc>>,
}

#[derive(Default)]
struct InMemoryTransactionStoreState {
    account_history_by_account: HashMap<DomainBankAccount, AccountHistory>,
}

#[derive(Clone, Default, Debug)]
pub struct InMemoryTransactionStore {
    state: Arc<Mutex<InMemoryTransactionStoreState>>,
}

impl InMemoryTransactionStore {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn new_with<I: IntoIterator<Item = TimestampedTransaction>>(
        bank_account: DomainBankAccount,
        initial_txns: I,
    ) -> Self {
        let txns_vec = initial_txns.into_iter().collect();
        let mut account_history_by_account = HashMap::<DomainBankAccount, AccountHistory>::new();
        account_history_by_account.insert(
            bank_account,
            AccountHistory {
                txns: txns_vec,
                last_ingest_time: None,
            },
        );
        let state = InMemoryTransactionStoreState {
            account_history_by_account,
        };
        Self {
            state: Arc::new(Mutex::new(state)),
        }
    }

    pub async fn count(&self, bank_account: &DomainBankAccount) -> usize {
        let state = self.state.lock().await;
        state
            .account_history_by_account
            .get(bank_account)
            .map_or(0, |account_history| account_history.txns.len())
    }
}

#[async_trait]
impl TransactionStoreInsertClient for InMemoryTransactionStore {
    async fn insert(&self, ingestion_record: IngestionRecord) -> InsertResult<()> {
        let mut state = self.state.lock().await;
        let mut account_history = state
            .account_history_by_account
            .entry(ingestion_record.bank_account.clone())
            .or_insert_with(AccountHistory::default);
        let ingestion_time = ingestion_record.ingestion_time;
        account_history.txns.extend(
            ingestion_record
                .transactions
                .into_iter()
                .map(|txn| TimestampedTransaction {
                    transaction: txn,
                    timestamp: ingestion_time,
                })
                .collect::<Vec<_>>(),
        );
        account_history.last_ingest_time = Some(ingestion_record.ingestion_time);
        Ok(())
    }
}

#[async_trait]
impl TransactionStoreQueryClient for InMemoryTransactionStore {
    async fn contains_transaction_with_id(
        &self,
        bank_account: &DomainBankAccount,
        transaction_id: &str,
    ) -> QueryResult<bool> {
        let state = self.state.lock().await;

        let contains = state
            .account_history_by_account
            .get(bank_account)
            .map_or(false, |account_history| {
                account_history.txns.iter().any(
                    |TimestampedTransaction {
                         transaction,
                         timestamp: _,
                     }| transaction.get_transaction_id() == transaction_id,
                )
            });
        Ok(contains)
    }

    async fn get_last_ingest_time(&self, bank_account: &DomainBankAccount) -> QueryResult<Option<DateTime<Utc>>> {
        let state = self.state.lock().await;
        let last_ingest_time = state
            .account_history_by_account
            .get(bank_account)
            .and_then(|account_history| account_history.last_ingest_time);
        Ok(last_ingest_time)
    }

    async fn get_timestamped_txns_by_id(
        &self,
        bank_account: &DomainBankAccount,
        txn_ids: Vec<&str>,
    ) -> QueryResult<HashMap<String, TimestampedTransaction>> {
        let state = self.state.lock().await;
        let mut res = HashMap::new();
        if let Some(history) = state.account_history_by_account.get(bank_account) {
            for txn in history.txns.clone() {
                let txn_id = txn.transaction.get_transaction_id();
                if txn_ids.contains(&txn_id.as_str()) {
                    res.insert(
                        txn_id,
                        TimestampedTransaction {
                            transaction: txn.transaction,
                            timestamp: txn.timestamp,
                        },
                    );
                }
            }
        }
        Ok(res)
    }

    async fn query(
        &self,
        _bank_account: &DomainBankAccount,
        _date_range: &DateRange,
        _date_range_policy: DateRangePolicy,
    ) -> QueryResult<Vec<DomainTransaction>> {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn insert__state_change_is_consistent_across_clones_of_store() {
        // Given
        let store = InMemoryTransactionStore::new();
        let cloned_store = store.clone();
        let ingestion_record = IngestionRecord::test();
        let bank_account = ingestion_record.bank_account.clone();

        // When
        store.insert(ingestion_record).await.unwrap();

        // Then
        let store_count = store.count(&bank_account).await;
        let cloned_store_count = cloned_store.count(&bank_account).await;
        assert_eq!(store_count, 1);
        assert_eq!(store_count, cloned_store_count);
    }
}
