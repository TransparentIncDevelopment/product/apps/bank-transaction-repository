pub mod error;

use async_trait::async_trait;
use chrono::{DateTime, Utc};
use error::Result;
use std::{fmt::Debug, sync::Arc};

#[async_trait]
pub trait ForensicsStoreClient: Send + Sync + Debug {
    async fn insert_if_new(&self, bank_txn_id: String, synthetic_id: String, timestamp: &DateTime<Utc>)
        -> Result<bool>;
}

#[async_trait]
impl ForensicsStoreClient for Arc<dyn ForensicsStoreClient> {
    async fn insert_if_new(
        &self,
        bank_txn_id: String,
        synthetic_id: String,
        timestamp: &DateTime<Utc>,
    ) -> Result<bool> {
        self.as_ref().insert_if_new(bank_txn_id, synthetic_id, timestamp).await
    }
}

#[async_trait]
impl<T: ForensicsStoreClient> ForensicsStoreClient for Arc<T> {
    async fn insert_if_new(
        &self,
        bank_txn_id: String,
        synthetic_id: String,
        timestamp: &DateTime<Utc>,
    ) -> Result<bool> {
        self.as_ref().insert_if_new(bank_txn_id, synthetic_id, timestamp).await
    }
}
