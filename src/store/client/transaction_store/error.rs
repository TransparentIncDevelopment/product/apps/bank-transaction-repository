use super::{insert::error::Error as InsertError, query::error::Error as QueryError};
use thiserror::Error;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    InsertError(#[from] InsertError),
    #[error(transparent)]
    QueryError(#[from] QueryError),
}
