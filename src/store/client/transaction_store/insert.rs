use super::TransactionStoreClient;
use crate::domain::data::IngestionRecord;
use async_trait::async_trait;
use std::sync::Arc;

pub mod error;
use error::Result;

#[async_trait]
pub trait TransactionStoreInsertClient: Send + Sync {
    async fn insert(&self, ingestion_record: IngestionRecord) -> Result<()>;
}

#[async_trait]
impl TransactionStoreInsertClient for Arc<dyn TransactionStoreClient> {
    async fn insert(&self, ingestion_record: IngestionRecord) -> Result<()> {
        self.as_ref().insert(ingestion_record).await
    }
}

#[async_trait]
impl<T: TransactionStoreInsertClient> TransactionStoreInsertClient for Arc<T> {
    async fn insert(&self, ingestion_record: IngestionRecord) -> Result<()> {
        self.as_ref().insert(ingestion_record).await
    }
}
