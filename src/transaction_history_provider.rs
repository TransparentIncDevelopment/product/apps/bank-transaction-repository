mod bank_clients;
mod dependencies;

pub mod error;
pub mod history_puller;

use crate::{
    domain::data::{DomainBankAccount, DomainHistoryRequest},
    ingestor::bank_client::BankClient,
    periodic::{error::Result as PeriodicResult, Periodic, PeriodicHandleImpl},
    public_interface::{BankConfiguration, Transaction},
    store::client::transaction_store::{
        date_range::{DateRange, DateRangePolicy},
        TransactionStoreClient,
    },
    synchronizer::synchronizer_trait::Synchronizer,
    transaction_history_provider::{bank_clients::construct_bank_client, events::HistoryRequested},
};
use chrono::{DateTime, Utc};
use error::{Error, Result};
use history_puller::{
    error::{Error as HistoryPullerError, Result as HistoryPullerResult},
    HistoryPuller,
};
use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};
use xand_secrets::SecretKeyValueStore;

use crate::store::client::forensics_store::ForensicsStoreClient;
pub use dependencies::{ConcreteTransactionHistoryProviderDependencies, TransactionHistoryProviderDependencies};

#[cfg(test)]
pub mod tests;

pub struct TransactionHistoryProvider<
    StoreClient: TransactionStoreClient + Clone,
    Forensics: ForensicsStoreClient + Clone,
    Deps: TransactionHistoryProviderDependencies<StoreClient, Forensics>,
> {
    synchronizers_by_routing_number: HashMap<String, Deps::Synchronizer>,
    store: StoreClient,
}

impl<StoreClient, Forensics, Deps> TransactionHistoryProvider<StoreClient, Forensics, Deps>
where
    StoreClient: TransactionStoreClient + Clone,
    Forensics: ForensicsStoreClient + Clone,
    Deps: TransactionHistoryProviderDependencies<StoreClient, Forensics>,
{
    #[allow(clippy::needless_pass_by_value)]
    pub fn initialize(
        banks_config: &HashSet<BankConfiguration>,
        store_client: StoreClient,
        forensics_client: Forensics,
        deps: &Deps,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<Self> {
        let synchronizers = banks_config
            .iter()
            .map(|bank| {
                Self::build_synchronizer(
                    deps,
                    bank,
                    store_client.clone(),
                    forensics_client.clone(),
                    secret_store.clone(),
                )
                .map(|synchronizer| (bank.routing_number.clone(), synchronizer))
            })
            .collect::<Result<_, _>>()?;

        Ok(Self {
            synchronizers_by_routing_number: synchronizers,
            store: store_client,
        })
    }

    fn build_synchronizer(
        deps: &Deps,
        bank: &BankConfiguration,
        store_client: StoreClient,
        forensics_client: Forensics,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<Deps::Synchronizer> {
        let bank_client = construct_bank_client(&bank.bank, secret_store)?;
        let ingestor = deps.create_ingestor(bank_client, store_client, forensics_client);
        let accounts = bank.get_domain_bank_accounts();

        deps.create_synchronizer(&bank.sync_policy, ingestor, &bank.routing_number, accounts)
            .map_err(|err| Error::InitializationError(Box::new(err)))
    }

    async fn synchronize(&self, bank_account: &DomainBankAccount) -> HistoryPullerResult<()> {
        let routing_number = bank_account.get_routing_number();

        let synchronizer = self
            .synchronizers_by_routing_number
            .get(&routing_number)
            .ok_or(HistoryPullerError::MissingBank { routing_number })?;

        synchronizer.synchronize(&bank_account.clone()).await?;

        Ok(())
    }

    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        start: &DateTime<Utc>,
    ) -> HistoryPullerResult<Vec<Transaction>> {
        let query_result = self
            .store
            .query(
                bank_account,
                &DateRange::new_unterminated(*start),
                DateRangePolicy::Early,
            )
            .await
            .map(|transactions| transactions.into_iter().map(Into::into).collect())?;
        Ok(query_result)
    }
}

#[async_trait::async_trait]
impl<StoreClient, Forensics, Deps> HistoryPuller for TransactionHistoryProvider<StoreClient, Forensics, Deps>
where
    StoreClient: TransactionStoreClient + Clone,
    Forensics: ForensicsStoreClient + Clone,
    Deps: TransactionHistoryProviderDependencies<StoreClient, Forensics>,
{
    async fn get_history(&self, request: &DomainHistoryRequest) -> HistoryPullerResult<Vec<Transaction>> {
        tracing::info!(message = ?HistoryRequested { request });
        let bank_account = request.get_bank_account();
        self.synchronize(&bank_account).await?;
        self.query(&bank_account, &request.get_history_after()).await
    }
}

impl<StoreClient, Forensics, Deps> Periodic for TransactionHistoryProvider<StoreClient, Forensics, Deps>
where
    StoreClient: TransactionStoreClient + Clone,
    Forensics: ForensicsStoreClient + Clone,
    Deps: TransactionHistoryProviderDependencies<StoreClient, Forensics>,
    Deps::Synchronizer: Periodic,
{
    type Handle = PeriodicHandleImpl<
        <<Deps as TransactionHistoryProviderDependencies<StoreClient, Forensics>>::Synchronizer as Periodic>::Handle,
    >;
    fn start_periodic_synchronization(&self) -> PeriodicResult<Self::Handle> {
        let handles = self
            .synchronizers_by_routing_number
            .iter()
            .map(|(_, synchronizer)| synchronizer.start_periodic_synchronization())
            .collect::<Result<_, _>>()?;
        tracing::info!(message = ?events::PeriodicSynchronizationStarted());
        Ok(PeriodicHandleImpl::new(handles))
    }
}

mod events {
    use crate::domain::data::DomainHistoryRequest;

    #[derive(Debug)]
    pub struct HistoryRequested<'a> {
        pub request: &'a DomainHistoryRequest,
    }

    #[derive(Debug)]
    pub struct PeriodicSynchronizationStarted();
}
