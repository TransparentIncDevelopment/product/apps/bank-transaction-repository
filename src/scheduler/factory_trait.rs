pub mod error;

use crate::{domain::data::DomainBankAccount, ingestor::IngestionHandle, scheduler::scheduler_trait::Scheduler};
use error::Result;

pub trait SchedulerFactory {
    type Scheduler: Scheduler;

    fn create(&self, ingestor: IngestionHandle, bank_account: &DomainBankAccount) -> Result<Self::Scheduler>;
}
