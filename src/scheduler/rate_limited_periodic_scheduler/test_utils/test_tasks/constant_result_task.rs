use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use async_trait::async_trait;

#[derive(Debug, Clone)]
pub struct ConstantResultTask<T> {
    result: T,
}

impl ConstantResultTask<()> {
    pub const fn void() -> Self {
        Self { result: () }
    }
}

#[async_trait]
impl<T: Clone + Send + Sync> CoordinatedTask for ConstantResultTask<T> {
    type Output = T;

    async fn execute(&self) -> Self::Output {
        self.result.clone()
    }
}

impl<T: Clone + Send + Sync> ConstantResultTask<T> {
    pub fn new(result: T) -> Self {
        Self { result }
    }
}
