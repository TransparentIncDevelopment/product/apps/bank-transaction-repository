use super::ConstantResultTask;
use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use std::sync::{
    atomic::{AtomicU32, Ordering},
    Arc,
};
use tokio::sync::Notify;

#[derive(Clone)]
pub struct ObservableTask<Task: CoordinatedTask> {
    inner: Task,
    num_calls: Arc<AtomicU32>,
    notifier: Arc<Notify>,
}

impl<Task: CoordinatedTask> ObservableTask<Task> {
    pub fn new(task: Task) -> Self {
        Self {
            inner: task,
            num_calls: Arc::default(),
            notifier: Arc::default(),
        }
    }

    pub fn was_called(&self) -> bool {
        self.num_calls() > 0
    }

    pub fn num_calls(&self) -> u32 {
        self.num_calls.load(Ordering::SeqCst)
    }

    pub fn reset_called(&self) {
        self.num_calls.store(0, Ordering::SeqCst);
    }

    pub async fn wait_for_call(&self) {
        self.notifier.notified().await;
    }
}

#[async_trait::async_trait]
impl<Task: CoordinatedTask> CoordinatedTask for ObservableTask<Task> {
    type Output = Task::Output;

    async fn execute(&self) -> Self::Output {
        self.num_calls.fetch_add(1, Ordering::SeqCst);
        self.notifier.notify_one();
        self.inner.execute().await
    }
}

impl Default for ObservableTask<ConstantResultTask<()>> {
    fn default() -> Self {
        Self::new(ConstantResultTask::void())
    }
}
