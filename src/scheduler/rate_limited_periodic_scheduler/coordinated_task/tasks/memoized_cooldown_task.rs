use crate::scheduler::rate_limited_periodic_scheduler::{
    coordinated_task::tasks::memoized_cooldown_task::events::NewOutputReturned, countdown_timer::CountdownTimer,
    CoordinatedTask,
};
use async_trait::async_trait;
use events::CachedOutputReturned;
use futures::lock::Mutex;

struct CooldownState<Timer, Task>
where
    Timer: CountdownTimer + Sync + Send,
    Task: CoordinatedTask,
{
    timer: Timer,
    task: Task,
    last_result: Option<Task::Output>,
}

impl<Timer, Task> CooldownState<Timer, Task>
where
    Timer: CountdownTimer + Sync + Send,
    Task: CoordinatedTask,
{
    pub fn get_cached_result(&self) -> Option<Task::Output> {
        if self.timer.is_expired() {
            None
        } else {
            self.last_result.clone()
        }
    }

    pub fn set_cached_result(&mut self, result: Task::Output) {
        self.last_result = Some(result);
    }

    pub async fn compute_new_result(&mut self) -> Task::Output {
        let result = self.task.execute().await;
        self.timer.reset();
        self.set_cached_result(result.clone());
        result
    }
}

/// A `CoordinatedTask` wrapper for an inner task which caches results of each execution and executes the inner task only
/// as frequently as allowed by a provided timer.
///
/// Executions are "serialized", i.e. concurrent execution of the inner task is impossible.
///
/// An execution blocks until there is no other concurrent execution of this task running. Then:
///   - If the cooldown timer is in "active" (non-expired) state -- i.e., we are within the cooldown period: Returns the
///     result of the last execution of this task.
///   - Otherwise: Executes the task again and returns the new result.
pub struct MemoizedCooldownTask<Timer, Task>
where
    Timer: CountdownTimer + Send + Sync,
    Task: CoordinatedTask,
{
    state: Mutex<CooldownState<Timer, Task>>,
}

#[async_trait]
impl<Timer, Task> CoordinatedTask for MemoizedCooldownTask<Timer, Task>
where
    Timer: CountdownTimer + Send + Sync,
    Task: CoordinatedTask,
{
    type Output = Task::Output;
    async fn execute(&self) -> Self::Output {
        let mut state_lock = self.state.lock().await;

        if let Some(cached_result) = state_lock.get_cached_result() {
            tracing::trace!(message = ?CachedOutputReturned());
            cached_result
        } else {
            tracing::trace!(message = ?NewOutputReturned());
            state_lock.compute_new_result().await
        }
    }
}

impl<Timer, Task> MemoizedCooldownTask<Timer, Task>
where
    Timer: CountdownTimer + Send + Sync,
    Task: CoordinatedTask,
{
    pub fn create(timer: Timer, task: Task) -> Self {
        Self {
            state: Mutex::new(CooldownState {
                timer,
                task,
                last_result: None,
            }),
        }
    }
}

mod events {
    #[derive(Debug)]
    pub struct CachedOutputReturned();

    #[derive(Debug)]
    pub struct NewOutputReturned();
}

#[cfg(test)]
mod cooldown_tests {
    use std::sync::atomic::{AtomicBool, Ordering};

    use tokio::runtime::Runtime;
    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;

    use crate::scheduler::rate_limited_periodic_scheduler::{
        coordinated_task::{
            tasks::memoized_cooldown_task::events::{CachedOutputReturned, NewOutputReturned},
            CoordinatedTask,
        },
        countdown_timer::CountdownTimer,
        test_utils::{
            test_tasks::{CallbackTask, ConstantResultTask, GatedTask, ObservableTask, UniqueResultTask},
            test_timer::{TestTimer, TimerState},
        },
    };

    use super::MemoizedCooldownTask;

    impl<Timer, Task> MemoizedCooldownTask<Timer, Task>
    where
        Timer: CountdownTimer + Send + Sync,
        Task: CoordinatedTask,
    {
        async fn test_set_cached_value(&mut self, value: Task::Output) {
            self.state.lock().await.set_cached_result(value);
        }
    }

    #[tokio::test]
    async fn execute__runs_task_when_cooldown_expired() {
        // Given
        const EXPECTED_VALUE: &str = "some_new_value";
        let timer = TestTimer::create(TimerState::Expired);
        let task = ConstantResultTask::new(EXPECTED_VALUE);
        let mut subject = MemoizedCooldownTask::create(timer.clone(), task.clone());
        subject.test_set_cached_value("old_Value").await;

        timer.set_state(TimerState::Expired);

        // When execute is called
        let call2 = subject.execute().await;

        // Then the value should be the value from the task
        assert_eq!(EXPECTED_VALUE, call2);
    }

    #[test]
    fn execute__emits_event_indicating_new_result_when_cooldown_expired() {
        // Given
        const EXPECTED_VALUE: &str = "some_new_value";
        let rt = Runtime::new().unwrap();
        let timer = TestTimer::create(TimerState::Expired);
        let task = ConstantResultTask::new(EXPECTED_VALUE);
        let mut subject = MemoizedCooldownTask::create(timer.clone(), task.clone());
        rt.block_on(subject.test_set_cached_value("old_Value"));

        timer.set_state(TimerState::Expired);

        // When execute is called
        let events = tracing_capture_event_fields!({
            rt.block_on(subject.execute());
        });

        // Then event emitted
        let expected_event: Vec<(String, String)> = vec![("message".into(), NewOutputReturned().debug_fmt())];
        assert!(events.contains(&expected_event), "{:?}", events);
    }

    #[tokio::test]
    async fn execute__resets_timer_on_task_execution_end() {
        // Given an expired timer
        let timer = TestTimer::create(TimerState::Expired);
        let task = ConstantResultTask::new("value");
        let mut subject = MemoizedCooldownTask::create(timer.clone(), task.clone());
        subject.test_set_cached_value("old_Value").await;

        // When execute is called
        subject.execute().await;

        // Then the timer is reset
        assert!(timer.was_reset());
    }

    #[tokio::test]
    async fn execute__does_not_reset_timer_on_task_execution_begin() {
        // Given an expired timer
        let timer_not_reset = AtomicBool::new(false);
        let timer = TestTimer::create(TimerState::Expired);
        let task = CallbackTask::new(|| timer_not_reset.store(timer.is_expired(), Ordering::SeqCst));
        let subject = MemoizedCooldownTask::create(timer.clone(), task.clone());

        // When execute is in progress
        subject.execute().await;

        // Then the timer should not have been reset during execution
        assert!(timer_not_reset.load(Ordering::SeqCst));
    }

    #[tokio::test]
    async fn execute__returns_previous_value_when_cooldown_active() {
        // Given
        let initial_value = u32::MAX;
        let timer = TestTimer::create(TimerState::Active);
        let task = ObservableTask::new(UniqueResultTask::default());
        let mut subject = MemoizedCooldownTask::create(timer.clone(), task.clone());
        subject.test_set_cached_value(initial_value).await;

        // When execute is called
        let call2 = subject.execute().await;

        // Then the returned value should be the same
        assert_eq!(initial_value, call2);

        // And the task should not be called
        assert!(!task.was_called());
    }

    #[test]
    fn execute__emits_event_indicating_cached_result_when_cooldown_active() {
        // Given
        let rt = Runtime::new().unwrap();
        let initial_value = u32::MAX;
        let timer = TestTimer::create(TimerState::Active);
        let task = ObservableTask::new(UniqueResultTask::default());
        let mut subject = MemoizedCooldownTask::create(timer, task);
        rt.block_on(subject.test_set_cached_value(initial_value));

        // When execute is called
        let events = tracing_capture_event_fields!({
            rt.block_on(subject.execute());
        });

        // Then event emitted
        let expected_event: Vec<(String, String)> = vec![("message".into(), CachedOutputReturned().debug_fmt())];
        assert!(events.contains(&expected_event), "{:?}", events);
    }

    #[tokio::test]
    async fn execute__runs_task_when_no_previous_value_and_cooldown_active() {
        // Given an active timer
        let timer = TestTimer::create(TimerState::Active);
        let task = ObservableTask::default();
        let subject = MemoizedCooldownTask::create(timer.clone(), task.clone());

        // When execute is called
        subject.execute().await;

        // And the task should be called
        assert!(task.was_called());
    }

    #[tokio::test]
    async fn execute__only_runs_one_task_at_a_time() {
        // Given
        let timer = TestTimer::create(TimerState::Active);
        let counter = ObservableTask::default();
        let guard_task = GatedTask::new(counter.clone());
        let task = ObservableTask::new(guard_task.clone());
        let subject = Box::leak(Box::new(MemoizedCooldownTask::create(timer.clone(), task.clone())));

        let guard = guard_task.block_tasks().await;

        // When two tasks are executed at once
        let _call1 = tokio::spawn(subject.execute());
        let _call2 = tokio::spawn(subject.execute());

        tokio::task::yield_now().await;
        task.wait_for_call().await;
        tokio::task::yield_now().await;

        // Then the task is only executed once
        assert_eq!(counter.num_calls(), 1);
        drop(guard);
    }
}
