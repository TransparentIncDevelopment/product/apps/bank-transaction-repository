use super::CountdownTimer;
use std::time::Duration;
use tokio::time::{Duration as TokioDuration, Instant};

/// A `CountdownTimer` operating on "real" time.
///
/// The timer is initialized with a duration, and can be reset multiple times before or after expiry.
///
/// Time is retrieved from a monotonic system clock. Its time source is equivalent to `std::time::Instant`. No absolute
/// time values or measurements are exposed.
#[derive(Debug)]
pub struct ClockCountdownTimer {
    end_time: Instant,
    duration: TokioDuration,
}

impl ClockCountdownTimer {
    pub fn new(duration: Duration) -> Self {
        Self {
            duration,
            end_time: Instant::now(),
        }
    }
}

#[async_trait::async_trait]
impl CountdownTimer for ClockCountdownTimer {
    fn reset(&mut self) {
        self.end_time = Instant::now() + self.duration;
    }

    fn is_expired(&self) -> bool {
        self.end_time <= Instant::now()
    }

    async fn wait_for_expiration(&self) {
        tokio::time::sleep_until(self.end_time).await;
    }
}
