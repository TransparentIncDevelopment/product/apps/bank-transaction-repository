use super::{PeriodicTaskTimer, PeriodicTaskTimerHandle, ThreadedPeriodicTaskTimer};
use crate::scheduler::rate_limited_periodic_scheduler::test_utils::{
    test_tasks::{ConstantResultTask, GatedTask, ObservableTask},
    test_timer::{TestTimer, TimerState},
};
use futures::Future;
use std::time::Duration;

const TEST_TIMEOUT: Duration = Duration::from_millis(100);

#[async_trait::async_trait]
trait TimeoutFuture: Future + Send {
    #[must_use]
    async fn with_timeout(self, duration: Duration) -> Result<Self::Output, tokio::time::error::Elapsed>;
}

#[async_trait::async_trait]
impl<F> TimeoutFuture for F
where
    F: Future + Send,
{
    async fn with_timeout(self, duration: Duration) -> Result<Self::Output, tokio::time::error::Elapsed> {
        tokio::time::timeout(duration, self).await
    }
}

#[tokio::test]
async fn execute__when_active_does_not_call() {
    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();

    // When task is started
    let _handle = timer.start(Box::new(task.clone()));
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // Then
    assert!(!task.was_called());
}

#[tokio::test]
async fn execute__when_expired_called_immediately_with_zero_delay() {
    // Given an expired timer
    let countdown = TestTimer::create(TimerState::Expired);
    let timer = ThreadedPeriodicTaskTimer::new(countdown);
    let task = ObservableTask::default();

    // When task is started
    let _handle = timer.start(Box::new(task.clone()));

    // Then the task is called
    assert!(task.wait_for_call().with_timeout(TEST_TIMEOUT).await.is_ok());
}

#[tokio::test]
async fn execute__when_timer_expires_the_task_is_called() {
    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();
    // And a running task
    let _handle = timer.start(Box::new(task.clone()));
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // When timer expires
    countdown.set_state(TimerState::Expired);

    // Then the task is called
    assert!(task.wait_for_call().with_timeout(TEST_TIMEOUT).await.is_ok());

    // And the timer is reset
    assert!(countdown.was_reset());
}

#[tokio::test]
async fn reset__does_not_call_task() {
    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();
    // And a running task
    let handle = timer.start(Box::new(task.clone()));
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // When handle is reset
    handle.reset();
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // Then the task is not called
    assert!(!task.was_called());
}

#[tokio::test]
async fn execute__when_task_is_still_executing_no_new_tasks_are_run() {
    // Given an active timer
    let countdown = TestTimer::create(TimerState::Expired);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let gated_task = GatedTask::new(ConstantResultTask::void());
    let task = ObservableTask::new(gated_task.clone());
    // And a running, blocked task
    let guard = gated_task.block_tasks().await;
    let _handle = timer.start(Box::new(task.clone()));
    assert!(task.wait_for_call().with_timeout(TEST_TIMEOUT).await.is_ok());
    task.reset_called();

    // When timer expires again
    countdown.set_state(TimerState::Active);
    tokio::task::yield_now().await;
    countdown.set_state(TimerState::Expired);
    tokio::task::yield_now().await;

    // Then the task is not called
    assert!(!task.was_called());
    drop(guard);
}

#[tokio::test]
async fn reset__resets_countdown_timer() {
    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ConstantResultTask::void();
    // And a running task
    let handle = timer.start(Box::new(task.clone()));
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // When handle is asked to reset
    handle.reset();

    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();

    // Then the timer was reset
    assert!(countdown.was_reset());
}

#[tokio::test]
async fn execute__multiple_expirations_run_task_multiple_times() {
    const EXPECTED_CALLS: u32 = 2;

    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();
    // And a running task
    let _handle = timer.start(Box::new(task.clone()));

    // When timer expires multiple times
    for _ in 1..=EXPECTED_CALLS {
        countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();
        countdown.set_state(TimerState::Expired);
        task.wait_for_call().with_timeout(TEST_TIMEOUT).await.unwrap();
    }

    // Then the task was invoked that many times
    assert_eq!(task.num_calls(), EXPECTED_CALLS);
}

#[tokio::test]
async fn execute__dropping_handle_stops_timer() {
    const EXPECTED_CALLS: u32 = 1;

    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();
    // And a running task
    let handle = timer.start(Box::new(task.clone()));

    // When timer expires
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();
    countdown.set_state(TimerState::Expired);
    task.wait_for_call().with_timeout(TEST_TIMEOUT).await.unwrap();

    // And we drop the handle
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();
    drop(handle);

    // And the timer expires again
    countdown.set_state(TimerState::Expired);

    // Then the task is not invoked
    task.wait_for_call().with_timeout(TEST_TIMEOUT).await.unwrap_err();
    assert_eq!(task.num_calls(), EXPECTED_CALLS);
}

#[tokio::test]
async fn execute__calling_stop_stops_timer() {
    const EXPECTED_CALLS: u32 = 1;

    // Given an active timer
    let countdown = TestTimer::create(TimerState::Active);
    let timer = ThreadedPeriodicTaskTimer::new(countdown.clone());
    let task = ObservableTask::default();
    // And a running task
    let handle = timer.start(Box::new(task.clone()));

    // When timer expires
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();
    countdown.set_state(TimerState::Expired);
    task.wait_for_call().with_timeout(TEST_TIMEOUT).await.unwrap();

    // And we call stop()
    countdown.wait_for_waiter().with_timeout(TEST_TIMEOUT).await.unwrap();
    handle.stop();

    // And the timer expires again
    countdown.set_state(TimerState::Expired);

    // Then the task is not invoked
    task.wait_for_call().with_timeout(TEST_TIMEOUT).await.unwrap_err();
    assert_eq!(task.num_calls(), EXPECTED_CALLS);
}
