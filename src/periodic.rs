pub mod error;

use error::Result;
use std::fmt::Debug;

pub trait Periodic {
    type Handle: PeriodicHandle;
    fn start_periodic_synchronization(&self) -> Result<Self::Handle>;
}

pub trait PeriodicHandle: Debug {}

impl<T: Debug> PeriodicHandle for T {}

#[derive(Debug)]
pub struct PeriodicHandleImpl<H: Debug> {
    /// This field is never read but may be needed if `H` has drop code that should run only when
    /// `PeriodicHandleImpl` is dropped.
    #[allow(dead_code)]
    handles: Vec<H>,
}

impl<H: Debug> PeriodicHandleImpl<H> {
    pub fn new(handles: Vec<H>) -> Self {
        Self { handles }
    }
}
